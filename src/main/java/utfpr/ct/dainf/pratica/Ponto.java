package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    
    private double x, y, z;
    public String nome_da_classe = getNome();
    
    public Ponto(){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Ponto(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }
    
    public double getZ() {
        return this.z;
    }
    
    public void setX() {
        this.x = x;
    }
    
    public void setY() {
        this.y = y;
    }
    
    public void setZ() {
        this.z = z;
    }
    
    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }
    
    public double dist(Ponto p)
    {
        return Math.sqrt(Math.pow(p.x-this.x,2)+Math.pow(p.y-this.y,2)+Math.pow(p.z-this.z,2));
    }
    
   public String toString()
    {
        return this.getNome()+"("+x+","+y+","+z+")";
    }
    
    public boolean equals(Ponto p)
    {
        if(this.x == p.x && this.y == p.y && this.z == p.z)
            return true;
        else
            return false;
    }
    
    
}
